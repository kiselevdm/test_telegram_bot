package main

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

type Chat struct {
	ChatId int64
	Bot    *tgbotapi.BotAPI
}

func (c *Chat) SendMessage(text string) {

	msg := tgbotapi.NewMessage(c.ChatId, text)
	c.Bot.Send(msg)
}

func GetNewChat(token string, chatId int64) *Chat {

	client := &http.Client{}

	if proxyURL != "" {
		proxy, _ := url.Parse(proxyURL)
		client.Transport = &http.Transport{Proxy: http.ProxyURL(proxy)}
	}
	bot, err := tgbotapi.NewBotAPIWithClient(token, client)
	if err != nil {
		log.Fatal(err)
	}

	chat := &Chat{
		ChatId: chatId,
		Bot:    bot,
	}

	return chat

}

func (c *Chat) GetFile() (*[]byte, error) {

	var File []byte
	fileOk := false

	for !fileOk {

		c.SendMessage("Отправьте мне текстовый файл со списком URL.")
		msg, err := c.getNextMessage()
		if err != nil {
			log.Println(err)
			return nil, err
		}

		if msg.Document != nil {

			FileUrl, _ := c.Bot.GetFileDirectURL(msg.Document.FileID)

			resp, err := c.Bot.Client.Get(FileUrl)
			if err != nil {
				log.Println(err)
				return nil, err
			}
			defer resp.Body.Close()

			File, err = ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Println(err)
				return nil, err
			}
			fileOk = true
		} else {
			c.SendMessage("С файлом какие-то проблеммы, отправьте еще раз.")
		}

	}
	return &File, nil
}

func (c *Chat) GetProcessQty() (int, error) {

	var qty int
	QtyOk := false

	for !QtyOk {

		c.SendMessage("Отправьте колличество параллельных процессов проверки.")
		msg, err := c.getNextMessage()
		if err != nil {
			log.Println(err)
			return 0, err
		}

		qty, err = strconv.Atoi(msg.Text)
		if err != nil {
			log.Println(err)
			c.SendMessage("Введено неверное значение.")
		} else {
			QtyOk = true
		}
	}
	return qty, nil
}

func (c *Chat) getNextMessage() (*tgbotapi.Message, error) {

	u := tgbotapi.NewUpdate(offset)
	u.Timeout = 60

	MsgOk := false
	idx := 0
	updates := []tgbotapi.Update{}

	for !MsgOk {
		var err error
		updates, err = c.Bot.GetUpdates(u)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		for i, upd := range updates {
			if upd.Message != nil && upd.Message.Chat.ID == c.ChatId {
				MsgOk = true
				idx = i
				break
			}
		}
	}
	offset = updates[idx].UpdateID + 1
	msg := updates[idx].Message
	return msg, nil
}
