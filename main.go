package main

import (
	"io"
	"log"
	"os"
	"time"
)

var proxyURL string = "http://97.95.219.64:44978/"
var MyBotId string = "640109870:AAFkPy_dDbXUSJKh8_7yHt8zTVQeMg0JFxQ"
var MyChatId int64 = 317499197
var offset int

type MyUrl struct {
	URL       string
	SimbolQty int64
	err       error
}

func main() {

	InitLog("log.log")

	sdc := make(chan interface{})

	go StartBot(MyBotId, MyChatId, sdc)

	time.Sleep(10 * time.Minute)
	sdc <- "stop"

}

func InitLog(
	LogFilePath string) {

	file, err := os.OpenFile(LogFilePath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}

	multi := io.MultiWriter(file, os.Stdout)
	log.SetOutput(multi)

}
