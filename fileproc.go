package main

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
)

func GetUrlFromFile(file *[]byte, urls *[]*MyUrl) error {

	rd := bytes.NewReader(*file)

	//читаем построчно и добавляем структуру
	scanner := bufio.NewScanner(rd)
	for scanner.Scan() {

		*urls = append(*urls, &MyUrl{scanner.Text(), 0, nil})

	}

	if err := scanner.Err(); err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func StartUrlTest(c <-chan *MyUrl) {

	for url := range c {
		url.SimbolQty, url.err = GetUrlBobySymbolQty(url.URL)

	}

}

func GetUrlBobySymbolQty(url string) (int64, error) {

	var qty int64 = 0
	//дергаем ссылку
	//log.Printf("Following a link %s \n", url)
	resp, err := http.Get(url)
	if err != nil {
		//log.Printf("Incorrect URL %s \n", err)
		return qty, err
	}

	defer resp.Body.Close()

	//читаем тело документа
	log.Printf("Read a body doement \n")
	if resp.StatusCode == http.StatusOK {
		bodyBytes, err2 := ioutil.ReadAll(resp.Body)
		if err2 != nil {

			log.Printf("Document reading error %s \n", err2)
			return qty, err2
		}
		//считаем количество символов
		cnt := []rune(string(bodyBytes))
		qty = int64(len(cnt))
	}

	return qty, nil
}
