package main

import (
	"fmt"
	"log"
	"math"
	"sync"
)

func StartBot(token string, chatId int64, shutdownChannel chan interface{}) {

	for {

		select {
		case <-shutdownChannel:
			return
		default:
		}

		UrlTests := []*MyUrl{}

		chat := GetNewChat(token, chatId)
		//chat.Bot.Debug = true

		File, err := chat.GetFile()
		if err != nil {
			log.Println(err)
		}

		UserProcQty, err2 := chat.GetProcessQty()
		if err2 != nil {
			log.Println(err2)
		}

		err3 := GetUrlFromFile(File, &UrlTests)
		if err3 != nil {
			log.Println(err3)
		}

		ProcQty := math.Min(float64(UserProcQty), float64(len(UrlTests)))

		wg := sync.WaitGroup{}
		c := make(chan *MyUrl, 1)

		for i := 0; i < int(ProcQty); i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				StartUrlTest(c)
			}()
		}

		for _, url := range UrlTests {
			c <- url
		}
		close(c)
		wg.Wait()

		MsgText := "Результат обработки: \n"
		for _, url := range UrlTests {

			if url.err != nil {
				MsgText += fmt.Sprintf("%s - %s \n", url.URL, url.err)
			} else {

				MsgText += fmt.Sprintf("%s - %d \n", url.URL, url.SimbolQty)
			}
		}
		chat.SendMessage(MsgText)

	}

}
